
using System;
using System.Diagnostics;
using SimpleGPIO.Boards;

public class Listener : IDisposable
{
    public Action<DateTime, double, PowerType> OnEvent { get; set; }
    private RaspberryPi pi;
    private Stopwatch watch = new Stopwatch();
    private DateTime now;
    public Listener()
    {
        this.pi = new RaspberryPi();
        this.now = DateTime.Now;
        this.watch.Start();
    }
    public void Listen(PowerType useType)
    {

        switch(useType)
        {
            case PowerType.Active:
                this.ListenActive();
            break; 
            case PowerType.Inductive:
                this.ListenInductive();
            break; 
        }
    }

    private void ListenActive()
    {
        var pin = this.pi.GPIO17;
        pin.IOMode = SimpleGPIO.IO.IOMode.Read;
        pin.Direction = SimpleGPIO.IO.Direction.In;
        var initialize = true;
        var previous = this.watch.ElapsedMilliseconds;
        pin.OnPowerOn(() =>
        {
            if (!initialize)
            {
                var current = this.watch.ElapsedMilliseconds;
                var elapsed = (current - previous) / 1000.0;
                previous = current;
                var power = 3600.0 / elapsed;
                var timestamp = this.now + this.watch.Elapsed;
                this.OnEvent?.Invoke(timestamp, power, PowerType.Active);
            }
            else
            {
                previous = this.watch.ElapsedMilliseconds;
                initialize = false;
            }
        });
    }
    private void ListenInductive()
    {
        var pin = this.pi.GPIO18;
        pin.IOMode = SimpleGPIO.IO.IOMode.Read;
        pin.Direction = SimpleGPIO.IO.Direction.In;
        var initialize = true;
        var previous = this.watch.ElapsedMilliseconds;
        pin.OnPowerOn(() =>
        {
            if (!initialize)
            {
                var current = this.watch.ElapsedMilliseconds;
                var elapsed = (current - previous) / 1000.0;
                previous = current;
                var power = 3600.0 / elapsed;
                var timestamp = this.now + this.watch.Elapsed;
                this.OnEvent?.Invoke(timestamp, power, PowerType.Inductive);
            }
            else
            {
                previous = this.watch.ElapsedMilliseconds;
                initialize = false;
            }
        });
    }

    #region IDisposable Support
    private bool disposedValue = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects).
            }
            this.pi.Dispose();
            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.

            disposedValue = true;
        }
    }

    // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    ~Listener()
    {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(false);
    }

    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
        // TODO: uncomment the following line if the finalizer is overridden above.
        // GC.SuppressFinalize(this);
    }
    #endregion
}