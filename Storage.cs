using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

public class StorageContext : DbContext
{
    public string Filename { get; private set; } = "power.db";

    public DbSet<PowerEvent> PowerEvents { get; set; }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=" + this.Filename);
    }
    public StorageContext()
    {
    }
    public StorageContext(string filename)
    {
        this.Filename = filename;
    }
}

