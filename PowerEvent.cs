using System;

public class PowerEvent
{
    public int Id { get; set; }
    public string Timestamp {get; set;}
    public double Power {get; set;}
    public PowerType PowerType {get; set;}
    public PowerEvent()
    {
        
    }
    public PowerEvent(string timestamp, double power, PowerType powerType)
    {
        this.Timestamp = timestamp;
        this.Power = power;
        this.PowerType = powerType;
    }
    public override string ToString() => $"Type {this.PowerType} Time {this.Timestamp} Power {this.Power}"; 
}