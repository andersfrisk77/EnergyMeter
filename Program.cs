﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using SimpleGPIO.Boards;

namespace EnergyMeter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Args: [active, inductive]  [true, false]");
            var useListener = PowerType.Active;
            if(args.Length >= 1)
            {
                Enum.TryParse<PowerType>(args[0], true, out useListener);
            }
            var useDB = false;
            if(args.Length >= 2)
            {
                bool.TryParse(args[1], out useDB);
            }
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var defaultFilename = Path.Join(baseDir, "power.db");
            var dbFilename = defaultFilename;
            Console.WriteLine($"Listen to {useListener}");
            Console.WriteLine($"Use database {useDB}");
            Console.WriteLine($"Database filename {dbFilename}");
            StorageContext db = null;
            if(useDB)
            {
                db = new StorageContext(dbFilename);
            }
            using (var listener = new Listener())
            {
                listener.Listen(useListener);
                listener.OnEvent += (t, p, a) =>
                {
                    Console.WriteLine($"{a}. Timestamp {t.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz")}, Power {p}");
                    db.PowerEvents.Add(new PowerEvent(t.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz"), p, a));
                    db.SaveChanges();
                };
                System.Threading.Thread.Sleep(-1);
            }
            db?.Dispose();
        }
    }
}
